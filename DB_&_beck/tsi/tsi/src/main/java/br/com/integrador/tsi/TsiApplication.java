package br.com.integrador.tsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsiApplication.class, args);
	}

}
